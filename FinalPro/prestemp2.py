import Adafruit_BMP.BMP085 as BMP085
from time import sleep 
import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM)     # set up BCM GPIO numbering
GPIO.setup(27, GPIO.OUT, initial=0)   # set GPIO27 as an output (LED)
GPIO.setup(25, GPIO.OUT, initial=0)   # set GPIO22 as an output (LED)

# Default constructor will pick a default I2C bus.
# Optionally you can override the bus number:
#sensor = BMP085.BMP085(busnum=2)
sensor = BMP085.BMP085()
print 'Temp = {0:0.2f} *C'.format(sensor.read_temperature())
print 'Pressure = {0:0.2f} Pa'.format(sensor.read_pressure())
print 'Altitude = {0:0.2f} m'.format(sensor.read_altitude())
print 'Sealevel Pressure = {0:0.2f} Pa'.format(sensor.read_sealevel_pressure())

print 'Temp = {0:0.2f} *C'.format(sensor.read_temperature())
print 'Pressure = {0:0.2f} Pa'.format(sensor.read_pressure())
print 'Altitude = {0:0.2f} m'.format(sensor.read_altitude())
print 'Sealevel Pressure = {0:0.2f} Pa'.format(sensor.read_sealevel_pressure())

try:
    while True:
        temp = sensor.read_temperature()
        print 'Temp = {0:0.2f} *C'.format(sensor.read_temperature())
        #print temp
        if temp > 32.5:
            print 'higher'
            GPIO.output(25, 0)
            GPIO.output(27, 1) 
        else:
            print 'low' 
            GPIO.output(27, 0) 
            GPIO.output(25, 1)
        sleep(1) 
except KeyboardInterrupt: 
    print 'done'
    GPIO.cleanup()       # clean up GPIO on CTRL+C exit  
finally:
    GPIO.cleanup()           # clean up GPIO on normal exit     
