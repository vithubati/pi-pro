import RPi.GPIO as GPIO
from time import sleep     # this lets us have a time delay (see line 12)
GPIO.setmode(GPIO.BCM)     # set up BCM GPIO numbering
GPIO.setup(23, GPIO.IN, pull_up_down = GPIO.PUD_UP)    # set GPIO23 as input (button)
GPIO.setup(24, GPIO.IN, pull_up_down = GPIO.PUD_DOWN)    # set GPIO24 as input (button)
GPIO.setup(27, GPIO.OUT)   # set GPIO27 as an output (LED)
GPIO.setup(22, GPIO.OUT)   # set GPIO22 as an output (LED)
try:
    while True:            # this will carry on until you hit CTRL+C
        if GPIO.wait_for_edge(24, GPIO.RISING): # if port 24 == 1
            print "Button 1 Pressed"
            GPIO.output(27, 1)         # set port/pin value to 1/HIGH/True
        if GPIO.wait_for_edge(24, GPIO.FALLING):
            print "Button 1 Released"
            GPIO.output(27, 0)         # set port/pin value to 0/LOW/False
        if GPIO.wait_for_edge(23, GPIO.FALLING):# if port 23 == 1
            print "Button 2 Pressed"
            GPIO.output(22, 1)         # set port/pin value to 1/HIGH/True
        if GPIO.wait_for_edge(23, GPIO.RISING):
            print "Button 2 Released"
            GPIO.output(22, 0)         # set port/pin value to 0/LOW/False
except KeyboardInterrupt:
    GPIO.cleanup()              
finally:                   # this block will run no matter how the try block exits
    GPIO.cleanup()         # clean up after yourself
    