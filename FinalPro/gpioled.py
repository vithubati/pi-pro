import RPi.GPIO as GPIO  
from time import sleep  
# here put all code for setting up GPIO,  
# for GPIO numbering, choose BCM  
GPIO.setmode(GPIO.BCM) 
GPIO.setmode(GPIO.BCM)  # set up BCM GPIO numbering  
GPIO.setup(23, GPIO.OUT) # set GPIO 23 as output 
GPIO.setup(24, GPIO.OUT) # set GPIO 23 as output 

try:  
    # here put your main loop or block of code
    while True:  
        GPIO.output(23, 1)         # set GPIO24 to 1/GPIO.HIGH/True  
        sleep(0.5)                 # wait half a second  
        GPIO.output(23, 0)         # set GPIO24 to 0/GPIO.LOW/False  
        sleep(0.5)                 # wait half a second 
        GPIO.output(24, 1)          
        sleep(0.5)                  
        GPIO.output(24, 0)          
        sleep(0.5)    
except KeyboardInterrupt:  # here put any code thst want to run before the program exits when you press CTRL+C  
  print "KeyboardInterrupt exception occurred!"  
except:  # this catches ALL other exceptions including errors. You won't get any error messages for debugging so only use it once your code is working
  print "Other error or exception occurred!"   
finally:
    print "cleaning"
    GPIO.cleanup() #resets all GPIO ports used by this program & this ensures a clean exit  