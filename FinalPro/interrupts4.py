#!/usr/bin/env python2.7
import RPi.GPIO as GPIO
from time import sleep
GPIO.setmode(GPIO.BCM)

GPIO.setup(23, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(17, GPIO.IN, pull_up_down=GPIO.PUD_UP)

GPIO.setup(27, GPIO.OUT, initial=0)   # set GPIO27 as an output (LED)
GPIO.setup(25, GPIO.OUT, initial=0)   # set GPIO22 as an output (LED)
GPIO.setup(24, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)

def my_callback(channel):
    print "falling edge detected on 17"
    GPIO.output(27, 1) 
    sleep(1)
    GPIO.output(27, 0)
    
def my_callback2(channel):
    print "falling edge detected on 23"
    GPIO.output(25, 1)
    sleep(1) 
    GPIO.output(25, 0) 
    
def my_callback3(channel):
    print "RISING edge detected on 17"
    GPIO.output(27, 1) 
    sleep(1)
    GPIO.output(27, 0) 

def my_callback4(channel):
    print "RISING edge detected on 23"
    GPIO.output(25, 1)
    sleep(1) 
    GPIO.output(25, 0) 
    
    
raw_input("Press Enter when ready\n>")
GPIO.add_event_detect(17, GPIO.FALLING, callback=my_callback, bouncetime=300)
GPIO.add_event_detect(23, GPIO.FALLING, callback=my_callback2, bouncetime=300)
#Button release
#GPIO.remove_event_detect(17)
#GPIO.add_event_detect(17, GPIO.RISING, callback=my_callback3, bouncetime=300)
#GPIO.remove_event_detect(23)
#GPIO.add_event_detect(23, GPIO.RISING, callback=my_callback4, bouncetime=300)

try:
    print "Waiting for rising edge on port 24"
    GPIO.wait_for_edge(24, GPIO.RISING)
    print "Rising edge detected on port 24. Here endeth the third lesson."

except KeyboardInterrupt:
    GPIO.cleanup()       # clean up GPIO on CTRL+C exit
finally:
    GPIO.cleanup()           # clean up GPIO on normal exit
