import Adafruit_BMP.BMP085 as BMP085
from time import sleep 
# Default constructor will pick a default I2C bus.
# Optionally you can override the bus number:
#sensor = BMP085.BMP085(busnum=2)
sensor = BMP085.BMP085()
print 'Temp = {0:0.2f} *C'.format(sensor.read_temperature())
print 'Pressure = {0:0.2f} Pa'.format(sensor.read_pressure())
print 'Altitude = {0:0.2f} m'.format(sensor.read_altitude())
print 'Sealevel Pressure = {0:0.2f} Pa'.format(sensor.read_sealevel_pressure())

print 'Temp = {0:0.2f} *C'.format(sensor.read_temperature())
print 'Pressure = {0:0.2f} Pa'.format(sensor.read_pressure())
print 'Altitude = {0:0.2f} m'.format(sensor.read_altitude())
print 'Sealevel Pressure = {0:0.2f} Pa'.format(sensor.read_sealevel_pressure())

try:
    while True:
        temp = sensor.read_temperature()
        print 'Temp = {0:0.5f} *C'.format(sensor.read_temperature())
        if temp > 30.5:
            print 'higher'
        else:
            print 'low' 
        sleep(1) 
except KeyboardInterrupt: 
    print 'done'
    

#sample output
#Temp = 20.20 *C
#Pressure = 101667.00 Pa
#Altitude = -28.27 m
#Sealevel Pressure = 101665.00 Pa