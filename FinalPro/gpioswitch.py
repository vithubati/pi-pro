import RPi.GPIO as GPIO  
from time import sleep  
# here put all code for setting up GPIO,  
# for GPIO numbering, choose BCM 
GPIO.setmode(GPIO.BCM)     # set up BCM GPIO numbering
GPIO.setup(24, GPIO.OUT)   # set GPIO 23 as output 
GPIO.setup(25, GPIO.IN)    # set GPIO 25 as input (button) 
try:  
    # here put your main loop or block of code
    while True:      
        if GPIO.input(25): # if port 25 == 1  
            print "Port 25 is 1/HIGH/True - LED ON"  
            GPIO.output(24, 1)         # set port/pin value to 1/HIGH/True  
        else:  
            print "Port 25 is 0/LOW/False - LED OFF"  
            GPIO.output(24, 0)         # set port/pin value to 0/LOW/False  
        sleep(0.1)            
except KeyboardInterrupt:  # here put any code thst want to run before the program exits when you press CTRL+C  
  print "KeyboardInterrupt exception occurred!"  
except:  # this catches ALL other exceptions including errors. You won't get any error messages for debugging so only use it once your code is working
  print "Other error or exception occurred!"   
finally:
    print "cleaning"
    GPIO.cleanup() #resets all GPIO ports used by this program & this ensures a clean exit  